#!/usr/bin/python
import sqlite3 as sql
import glob
import os, sys
import cPickle
from sets import Set

con = sql.connect('SPEED.db')
cur = con.cursor()


#load entrez gene id to name conversion
fh = open('9606.name2gene')
name2gene, gene2name = cPickle.load(fh)
fh.close()


#keep list of genes in dictionary
genes = dict(cur.execute('SELECT * FROM Genes'))

##dirs = glob.glob('C:\Documents and Settings\Jiggy\My Documents\Berlin Research\Signaling Microarray data\*')
dirs = glob.glob(sys.argv[1] + '/*')
for dir in dirs:
    pathway = os.path.basename(dir)
##    if pathway in ['H2O2', 'IL-1']:
#    if pathway in ['H2O2', 'IL-1', 'Wnt', 'MAPK_only', 'PI3K_only','VEGF']:
#        print "bad", pathway
##    if pathway not in ['TGFB']:
#        continue
    
    if not os.path.isdir(dir):
        continue
    exp_dirs = glob.glob(dir + '/*')
    for exp_dir in exp_dirs:
        exp_id = os.path.basename(exp_dir)
        if not os.path.isdir(exp_dir):
            continue

        description_file = os.path.join(exp_dir, 'Description.txt')        
        gsm_file = os.path.join(exp_dir, 'gsm_accession.dat')        
        data_file = os.path.join(exp_dir, 'zvalues.dat')
        if not os.path.exists(data_file):
            continue

        #parse description
        fh = open(description_file)
        cell_line = ''
        for ch in fh.readline().strip().split('\t')[-1]:
            try:
                cell_line += unicode(ch)
            except:
                pass
        modification = ''
        for ch in fh.readline().strip().split('\t')[-1]:
            try:
                modification += unicode(ch)
            except:
                pass
        time = ''
        for ch in fh.readline().strip().split('\t')[-1]:
            try:
                time += unicode(ch)
            except:
                pass
        comment = ''
        for ch in ' '.join(fh.readline().strip().split()[1:]):
            try:
                comment += unicode(ch)
            except:
                pass
        fh.close()

        #parse_gsm
        controls = []
        stimuli = []
        fh = open(gsm_file)
        for line in fh:
            line = str(line.strip()).split()
            if line[1] == '1':
                controls.append(line[0])
            elif line[1] == '2':
                stimuli.append(line[0])
        controls = ','.join(controls)
        stimuli = ','.join(stimuli)
        fh.close()        

##        print '%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n\n' % (exp_id, pathway, controls, stimuli, cell_line, modification, time, comment)

        #insert into pathways
        try:
            cur.execute('INSERT INTO Pathways VALUES (?,?)', (pathway, pathway))
        except:
            print sys.exc_info()[1].message
        
        #insert experiment information into Experiments table
        try:
            cur.execute('INSERT INTO Experiments VALUES (?,?,?,?,?,?,?,?)', (exp_id, pathway, controls, stimuli, cell_line, modification, time, comment))
        except:
            print sys.exc_info()[1].message
    
        fh = open(data_file)
        fh.readline()
        print pathway, exp_id
        data = []
        for line in fh:
            if not line.strip():
                continue
            probe_id, gene_id, zvalue, expression = line.strip().split('\t')
            try:
                zvalue = float(zvalue)
            except:
                zvalue = 0
            try:
                expression = float(expression)
            except:
                expression = 0
            #take smallest gid if multiple genes for a probe
            gene_id = gene_id.split(' \\\ ')
            gene_id.sort()
            gene_id = gene_id[0]
            try:
                gene_id = int(gene_id)
            except:
                continue

            #convert to name
            try:
                gene_name = gene2name[str(gene_id)]
            except:
                gene_name = ''

##            print 'g', pathway, exp_id, gene_id, gene_name
            #Insert to Genes table            
            #add gene if not already there
            if gene_id not in genes:
                genes[gene_id] = gene_name
                cur.execute('INSERT INTO Genes VALUES (?,?)', (gene_id, gene_name))
            #replace gene if it is in genes without gene name and we are now supplying a gene name
            elif gene_name and not genes[gene_id]:
                genes[gene_id] = gene_name
                cur.execute('REPLACE INTO Genes VALUES (?,?)', (gene_id, gene_name))
            else:   #do nothing
                pass

            data.append([probe_id, gene_id, exp_id, expression, zvalue])            


        len_data = len(data)
        #sort by expression to make expression rank percent
        data = sorted(data, key=lambda x:x[3], reverse=True)
##        data = data[:int(round(50*len(data)/100))]
        data = [data[i]+[float(i+1)/len_data] for i in range(len_data)]

        #sort by zval and make zval rank percentage
        data = sorted(data, key=lambda x:abs(x[4]), reverse=True)
        data = [data[i]+[float(i+1)/len_data] for i in range(len_data)]
        
        #Insert to data table
        
        for vals in data:
##            print vals
            try:
                cur.execute('INSERT INTO Data VALUES (?,?,?,?,?,?,?)', vals)
            except:
                print sys.exc_info()[1].message
                print probe_id, gene_id, exp_id

            

        fh.close()        
        


con.commit()
