#!/usr/bin/python
import sqlite3 as sql

con = sql.connect('SPEED.db')
cur = con.cursor()
#Create Tables
cur.execute('CREATE TABLE Pathways (p_id TEXT PRIMARY KEY, pathway TEXT)')
cur.execute('CREATE TABLE Experiments (e_id TEXT PRIMARY KEY, p_id TEXT, controls_GSM TEXT, stimuli_GSM TEXT, cell_line TEXT, modification TEXT, time TEXT, comment TEXT)')
cur.execute('CREATE TABLE Genes (g_id INTEGER PRIMARY KEY, gene TEXT)')
cur.execute('CREATE TABLE Data (probe_id INTEGER, g_id INTEGER, e_id TEXT, \
            expression REAL, zvalue REAL, expression_percent REAL,  abs_zvalue_percent REAL, \
            PRIMARY KEY (probe_id, g_id, e_id))')
con.commit()
con.close()
