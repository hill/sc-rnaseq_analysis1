#!/usr/bin/python
import sqlite3 as sql
import glob
import os, sys


con = sql.connect('SPEED.db')
cur = con.cursor()



#Insert EGFR in to pathway database
##cur.execute('INSERT INTO Pathways VALUES (?,?,?)', [5, 'egfr', 'EGFR Signaling'])

#DELETE VEGF
#delete from pathways
pathway = 'VEGF'
cur.execute('DELETE FROM Pathways WHERE p_id=?', (pathway,))
#delete fom Experiments
exps = cur.execute('SELECT e_id FROM Experiments WHERE p_id = ?', (pathway,)).fetchall()
cur.execute('DELETE FROM Experiments WHERE p_id=?', (pathway,))
for exp in exps:
    #delete from data
    cur.execute('DELETE FROM Data WHERE e_id=?', (str(exp), ))

#REPLACE PI3K
#delete from pathways
#pathway = 'PI3K_only'
#cur.execute('DELETE FROM Pathways WHERE p_id=?', (pathway,))
#replace in Experiments
#exps = cur.execute('INSERT OR REPLACE INTO Experiments (p_id) VALUES (?) WHERE p_id = ?', ('MAPK_PI3K',pathway))


    
con.commit()
