import sqlite3 as sql
import glob
import os, sys


#Usage
#add_to_db.py file
#file has following structure:
#ExpID.replicate.perturbed_protein.pathway.data
#if there are multiple pathways
#ExpID.replicate.perturbed_protein.pathway1 pathway2.data
#Example
#add_to_db.py C:/GSE1492.rep1.ifng.jak.data

con = sql.connect('SPEED.db')
cur = con.cursor()


#keep list of pathways in dictionary

paths = {}
q = cur.execute('SELECT * FROM Pathways')
for row in q:
    paths[row[1]] = (row[0], row[2])




#automatically add the rest
#keep list of genes in dictionary
genes = dict(cur.execute('SELECT * FROM Genes'))

files = sys.argv[1:]

for file in files:
    print
    print os.path.basename(file)
    exp_id, replicate, factor, pathways = os.path.basename(file)[:-5].split('.')
    pathways = pathways.split(' ')
                

    #insert experiment information into Experiments table
    for path in pathways:
        if path not in paths:
            ui = raw_input('\nPathway not in database.\nDo you want to add %s to db (y/n):\n ' % path)
            if ui.lower() == 'y':
                name = raw_input('\nEnter Full Name for Pathway (optional):\n ')
                print "Adding %s to Pathways Table" % path
                path_int = max([val[0] for val in paths.values()]) + 1
                ins = [path_int, path, name]
                try:
                    cur.execute('INSERT INTO Pathways VALUES (?,?,?)', ins)
                except:
                    print sys.exc_info()[1].message                
        try:
            cur.execute('INSERT INTO Experiments VALUES (?,?,?,?,?)', (exp_id, os.path.basename(file)[:-5], factor, paths[path][0], ''))
        except:
            print sys.exc_info()[1].message
    
    fh = open(file)
    for line in fh:
        gid, gene_name, probe, value = line.strip().split('\t')
        gid = int(gid)

        #Insert to Genes table            
        #add gene if not already there
        if gid not in genes:
            genes[gid] = gene_name
            cur.execute('INSERT INTO Genes VALUES (?,?)', (gid, gene_name))
        #replace gene if it is in genes without gene name and we are now supplying a gene name
        elif gene_name and not genes[gid]:
            genes[gid] = gene_name
            cur.execute('REPLACE INTO Genes VALUES (?,?)', (gid, gene_name))
        else:   #do nothing
            pass
            
        #Insert to data table
        try:
            cur.execute('INSERT INTO Data VALUES (?,?,?,?,?)', (probe, exp_id, os.path.basename(file)[:-5], gid, float(value)))
        except:
            print sys.exc_info()[1].message

        

    fh.close()        
        


con.commit()