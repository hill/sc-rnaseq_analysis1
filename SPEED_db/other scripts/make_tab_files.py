#!/usr/bin/python
import sqlite3 as sql
import glob
import os, sys


con = sql.connect('SPEED.db')
cur = con.cursor()

tables = []
for info in cur.execute('SELECT * FROM sqlite_master WHERE type="table"'):
    tables.append(info[1])

for table in tables:
    print table
    data = cur.execute('SELECT * FROM %s' % table).fetchall()
    fields = [val[0] for val in cur.description]

    fh = open('%s.tab' % table, 'w')
    fh.write("%s\n" % '\t'.join(fields))
    for row in data:
        fh.write('%s\n' % '\t'.join([str(val) for val in row]))

    fh.close()    
    
    
##con.commit()

