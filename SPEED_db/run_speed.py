#!/usr/bin/python

##This file contains cgi code

#Imports
import cgi
import sys
from math import log10, log
import speed_functions as speed
from sets import Set

def print_header():
    """This function prints the main HTML content in the page

    """

    import random
    example_tgfb_genes = [102,240,393,677,1263,2323,2771,2956,3066,3303,3384,3422,3428,3725,4076,4602,4817,4953,5281,5292,5481,6504,7071,7531,7536,7852,8140,8317,8443,8453,8615,8655,8707,9453,10049,10051,10559,10614,23636,29127,51109,51128,54826,55032,55544,55726,55957,64135,64318,113791]
    example_mapk_pi3k_genes = [10140,1051,1490,154,1647,1843,1847,1848,1880,1906,1958,1959,1960,2152,22822,2303,2353,2354,23645,2669,3164,3491,3569,3576,3725,3726,3976,4170,4609,467,4929,5292,5328,5996,5997,602,6258,6347,6355,6446,6515,654,6722,6850,7128,7538,8013,8315,8535,8553,8870,9021,960]
    example_nfkb_genes = [58,302,597,960,1032,1509,2665,3091,3434,3553,3557,3576,4033,4717,4790,4791,5054,5265,5627,5741,6275,6280,6347,6348,6351,6648,7130,7277,7533,7832,11151,29126,54677,90410]
    example_tnfa_genes = [4609,9589,10318,25819,23516,4792,415116,7128,3569,2919,4772,84879,3659,3576,415116,3455,4794,4824,65108,4790,8767,3656,5328,8870,6648,6347,1027,7832,4814,2081,7130,7127,330,9308,6776,81788,84419,2643,56260,3383,2920,6364,608,29100,51312,8676,4791,1839,3096,5971,84870,6352,3627]
    example_il1_genes = [125,224,862,1028,1831,1948,2295,2535,2643,2919,2920,2921,3383,3553,3569,3576,3627,3669,4094,4128,4599,4824,4919,4938,5631,5743,6354,6355,6364,6648,6909,7127,7128,8309,10231,10826,11168,54796,64506,116039]
    example_jakstat_genes = [3434,6373,6373,3627,4600,8638,4599,8638,3433,4939,4938,4938,5698,23586,10561,6398,7098,10561,2633,3665,51296,4940,3430,8519,9830,2633,8519,2643,3431,24138,3431,80830,8743,3431,9473,24138,3431,6773]
    example_wnt_genes = [72,150,216,270,274,412,782,786,827,845,991,1000,1160,1278,1299,1306,1359,1410,1428,1468,1508,1573,1645,1805,1908,1909,2006,2013,2159,2170,2173,2192,2273,2326,2824,2952,3006,3316,3486,3488,4147,4151,4154,4619,4633,4634,4712,4756,4826,4892,5168,5224,5308,5588,5653,6307,6351,6362,6442,6622,6913,7135,7136,7139,7168,7756,7849,7941,8349,8404,8406,9452,9588,10109,10234,10500,11197,23676,25907,25975,26353,26548,26872,51201,51209,51299,51760,54209,55384,55603,55800,55893,57722,64388,79047,79191,79853,79933,80755,83604,84808,92346,93099,114801,114907,115286,116372,139596,140465,147495,221143,222166,252995]

    print "Content-Type: text/html\n\n"

    print """
<HEAD>
    <LINK href="/SPEED_style.css" rel="stylesheet" type="text/css">
    <LINK href="/favicon.ico" rel="shortcut icon"> 

<script type="text/javascript">
function fill_tgfb_genes()
{
document.getElementById("genes").value="%s";
}

function fill_mapk_pi3k_genes()
{
document.getElementById("genes").value="%s";
}

function fill_nfkb_genes()
{
document.getElementById("genes").value="%s";
}

function fill_tnfa_genes()
{
document.getElementById("genes").value="%s";
}

function fill_il1_genes()
{
document.getElementById("genes").value="%s";
}

function fill_jakstat_genes()
{
document.getElementById("genes").value="%s";
}

function fill_wnt_genes()
{
document.getElementById("genes").value="%s";
}




</script>

</HEAD>
  
<title>SPEED: Run Speed</title>
<body>
<h1 align=center>SPEED: (S)ignaling (P)athway (E)nrichment using (E)xperimental (D)atasets</h1>
</body>

<h2>
<table border=1 align="center" style="font-size:22px;" cellspacing=8>
<tr>
<td style="background-color:#eeeeff;"><a href="/" style="color:#333333;" >&nbsp;Home&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/cgi-bin/run_speed.py" style="color:#333333;" >&nbsp;Run SPEED&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/cgi-bin/download_annotations.py" style="color:#333333;" >&nbsp;Download Annotations&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/cgi-bin/database_statistics.py" style="color:#333333;" >&nbsp;Database Statistics&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/cgi-bin/suggest.py" style="color:#333333;" >&nbsp;Suggest Pathways&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/help.html" style="color:#333333;" >&nbsp;Help&nbsp;</a></td>
</tr>
</table>
</h2>

""" % ('\\n'.join([str(gene) for gene in example_tgfb_genes]), '\\n'.join([str(gene) for gene in example_mapk_pi3k_genes]), '\\n'.join([str(gene) for gene in example_nfkb_genes]), '\\n'.join([str(gene) for gene in example_tnfa_genes]), '\\n'.join([str(gene) for gene in example_il1_genes]), '\\n'.join([str(gene) for gene in example_jakstat_genes]), '\\n'.join([str(gene) for gene in example_wnt_genes]))

def print_tail():
    """This function prints the tail of the web page

    """

    print """
    <a href="http://www.sys-bio.net/">Group of Systems Biology of Molecular Network</a>. 
    Contact: <a href="nils.bluethgen@charite.de">Nils Bl&#252;thgen</a>.
    <br>
    Laboratory of Molecular Tumor Pathology (Charite) and the Institute of Theoretical Biology (HU Berlin).
    <br>

"""    


def print_main(input_list=[], zscore_percent=1, overlap=20, discard_percent=50, dload=0, bkgr_check=0, unique=0, incl_pathways=speed.default_pathways, format_gene_list=''):
    """This function prints the main HTML content in the page

    """

    dltext = ''
    bkgr_check_text = ''
    unique_text = ''

    if dload:
        dltext = 'checked'
    if bkgr_check:
        bkgr_check_text = 'checked'
    if unique:
        unique_text = 'checked'

    if not format_gene_list:
        format_gene_list = '\n'.join([str(gene) for gene in input_list])

    print """    

<p>
The SPEED enrichment algorithm extracts signature gene sets per pathway from microarray experiments and compares
an input gene list against the signature genes using <a href="http://en.wikipedia.org/wiki/Fishers_exact_test" target="_blank">Fisher's exact test</a>.
The reported p-value is a one-tailed calculation. 
Multiple hypothesis correction is achieved by calculating a false discovery rate described in <a href="http://www.ncbi.nlm.nih.gov/pubmed/15647509" target="_blank">Bl&#252;thgen et al, Nucleic Acids Res., 2005</a>.
</p>


<hr>
<FORM METHOD="POST" ACTION="/cgi-bin/run_speed.py#results" enctype="multipart/form-data">
<p align="center"><b>Please enter your gene list below (limited to 1000 IDs):</b>
<br><br>
Click buttons for sample lists:
<input type="button" value="TGFB e.g." onclick="fill_tgfb_genes()" />
<input type="button" value="MAPK_PI3K e.g." onclick="fill_mapk_pi3k_genes()" />
<input type="button" value="TLR e.g." onclick="fill_nfkb_genes()" />
<input type="button" value="TNFa e.g." onclick="fill_tnfa_genes()" />
<input type="button" value="IL1 e.g." onclick="fill_il1_genes()" />
<input type="button" value="JAK/STAT e.g." onclick="fill_jakstat_genes()" />
<input type="button" value="WNT e.g." onclick="fill_wnt_genes()" />
<br>
<br>
<TEXTAREA id="genes" NAME="genes" COLS="60" ROWS="10">%s</TEXTAREA>
<br>
""" % format_gene_list

    print "<table border=0>"
    all_pathways = incl_pathways + list(Set(speed.pathways.keys()).difference(incl_pathways))

    print '<tr><td>Pathways to include:</td><td><ul class="checklist">'
    for path in all_pathways:
        path_text = ''
        if path in incl_pathways:
            path_text = 'checked'
        print '<li><input type="checkbox" name="incl_pathways" value="%s" %s>%s</li>' % (path, path_text, path)

    print '</ul></td></tr>'


    print """

<tr>
<td>Max. absolute z-score percentile:</td>
<td><input type="text" name="zscore_percent" value=%s> &#37;</td>
</tr>
<tr>
<td>Min. percent overlap across experiments:</td>
<td><input type="text" name="overlap" value=%s> &#37;</td>
</tr>
<tr>
<td>Max. expression level percentile:</td>
<td><input type="text" name="discard_percent" value=%s> &#37;</td>
</tr>
<tr>
<td>Signature genes must be unique:</td>
<td><input type="checkbox" name="unique_check" value=1 %s></td>
</tr>
</table>
<br>
<input type="checkbox" name="dload" value=1 %s> Download results as tab delimited text<br>
<br>
<input type="checkbox" name="background_check" value=1 %s> Incl. Background List:
<input type="file" name="background_file" size=100>
<br><br>
<INPUT TYPE="submit" name="run">
</p>
</FORM>
<hr>

""" % (str(zscore_percent), str(overlap), str(discard_percent), unique_text, dltext, bkgr_check_text)






def print_results(gene_list, fishers, p_values, fdr, signature_genes_in_list, dload, zscore_percent, overlap, discard_percent, unique, bkgr_check, bkgr_file, incl_pathways, format_gene_list):
        
    pathways = fishers.keys()
    pathways.sort()
    
    if dload:
        print "Content-Type: text/plain\n"
        print """#Total genes in list: %d
#Total genes in background: %d
#
#Pathway\tGenes in List\tGenes in Background\tP-Value\tFDR""" % (fishers[pathways[0]][1], fishers[pathways[0]][3])

        for pathway in pathways:
            print "#%s\t%d\t%d\t%0.5g\t%0.5g" % (pathway[1] ,fishers[pathway][0], fishers[pathway][2], p_values[pathway], fdr[pathway])

        print "#"        
        print "#"
        print "#Pathway\tGene_ID\tGene_Name"
        for path in signature_genes_in_list:
            for gene, name in signature_genes_in_list[path]:
                print "%s\t%d\t%s" % (path[0], gene, name)           

        
        return
    
    print_header()
    print_main(gene_list, zscore_percent, overlap, discard_percent, dload, bkgr_check, unique, incl_pathways, format_gene_list)
   
    print """
<a name="results"></a>
<p align="center">
Total genes in list: %d
<br>
Total genes in background: %d
<br>
<br>
""" % (fishers[pathways[0]][1], fishers[pathways[0]][3])
    
    print """
<table border="0" cellspacing="0">
<tr style="background-color:#eeeeee;">
<th align="left" style="padding:5px 10px;">Pathway</th>
<th align="right" style="padding:5px 10px;">Genes in List</th>
<th align="right" style="padding:5px 10px;">Genes in Background</th>
<th align="right" style="padding:5px 10px;">P-Value</th>
<th align="right" style="padding:5px 10px;">FDR</th>
</tr>
""" 


    sorted_pathways = [(fdr[pathway], pathway) for pathway in pathways]
    sorted_pathways.sort()
    sorted_pathways = [p for f,p in sorted_pathways]
    odd = 1
    for pathway in sorted_pathways:
        odd += 1
        if odd%2:
            print '<tr style="background-color:#eeeeff;">'
        else:
            print "<tr>"
        print "<td style='padding:5px 10px;'>%s</td><td align='right' style='padding:5px 10px;'>%d</td><td align='right' style='padding:5px 10px;'>%d</td><td align='right' style='padding:5px 10px;'>%0.3g</td><td align='right' style='padding:5px 10px;'>%0.3g</td>" % (pathway[1] ,fishers[pathway][0], fishers[pathway][2], p_values[pathway], fdr[pathway])
        print "</tr>"
    print "</table></p>"

    
    #charts
    chart_labels = []
    chart_counts = []
    chart_fdrs = []
    fdr_vals = sorted(fdr.values())
    for p in sorted_pathways:
        if str(fishers[p][0]) in ["None", "0"]:
            continue
        elif not fdr[p]:
            chart_labels.append(p[0])
            chart_counts.append(str(fishers[p][0]))
            chart_fdrs.append(-1*log10(fdr_vals[1]))
        else:
            chart_labels.append(p[0])
            chart_counts.append(str(fishers[p][0]))
            chart_fdrs.append(-1*log10(fdr[p]))


    if chart_counts:
        chart_labels.reverse() 
        print "<p align='center'>"
        print """<table border="0" width=85% align="center">
<tr>
"""
        print "</td>"
        print "<td align=center style='vertical-align:top'>"
    ##        print "&nbsp;"*200
        
        max_fdr = max(chart_fdrs)
        if not max(chart_fdrs):
            max_fdr = 1.0
        scale_fdr = [f/max_fdr*100 for f in chart_fdrs]
        yscale = [0, max_fdr/3, 2*max_fdr/3, max_fdr]
       

        height = 350
        if len(chart_labels) <= 5:
            height = 250
        elif len(chart_labels) <= 8:
            height = 300
        print """<img src="http://chart.apis.google.com/chart?
chs=400x%d
&amp;chd=t:%s
&amp;cht=bhs
&amp;chco=6666ff
&amp;chxt=x,y
&amp;chxl=
0:|%s|
1:|%s|
&amp;chtt=-log10(FDR)+per+pathway
&amp;alt=FDR+per+pathway" />
""" % (height, ','.join([str(f) for f in scale_fdr]) , '|'.join("%0.3f" % y for y in yscale) , '|'.join(chart_labels))



        print """
<td align=center style='vertical-align:top'>"""


        chart_labels.reverse()      
        print """<img src="http://chart.apis.google.com/chart?
chs=500x250
&amp;chd=t:%s
&amp;chco=3333ff
&amp;chls=0,000000
&amp;cht=p
&amp;chl=%s
&amp;chtt=Signature+genes+in+list
&amp;alt=Signature+genes+pie+chart" />
""" % (','.join(chart_counts), '|'.join(chart_labels))


        print "</td></tr></table></p>"

    print "<br><br>"    
        
    print """<p align="center"><table border="0" cellspacing="0" width=85%>
<tr style="background-color:#eeeeee;">
<th align="left" style="padding:5px 10px;">Pathway</th>
<th align="center" style="padding:5px 10px;">Genes</th>
</tr>
"""


    odd = 1       
    for path in pathways:
        odd += 1
        if odd%2:
            print '<tr style="background-color:#eeeeff;">'
        else:
            print "<tr>"
        print "<td align='left' style='padding:5px 10px;'>%s</td><td align='left' style='padding:5px 10px;'>" % path[1]
        gene_links = ""
        for gene, name in signature_genes_in_list[path]:
            gene_links += "<a href='http://www.ncbi.nlm.nih.gov/gene/%d' target='_blank'>%d</a> (%s), " % (gene, gene, name)
        print gene_links[:-2]
        print "</td></tr>"
    
    print "</table></p>"
    



form = cgi.FieldStorage()


gene_list = []
conv_gene_list = []

if not form or not 'genes' in form:
    genes = []
else:
    genes = form['genes'].value.replace('\r', '').split('\n')

#if the first one is an integer then assume all entrez gene ids
is_int = 0
if genes:
    try:
        gene = int(genes[0].split(' ')[0])
        is_int = 1
    except:
        pass
        
if is_int:
    for gene in genes:
        if not gene:
            continue
        gene = gene.split(' ')[0]
        try:
            gene_list.append(int(gene))
        except:
            pass
else:
    for gene in genes:
        if not gene:
            continue
        gene = gene.split(' ')[0].split('.')[0]
        if gene:
            conv_gene_list.append(gene)

format_gene_list = ''
if conv_gene_list:
    if len(conv_gene_list) == 1:
        conv_gene_list.append('')
    format_gene_list, gene_list = speed.convert_gene(conv_gene_list)

                 
#for testing
##gene_list = [18,136,394,627,639,687,817,841,862,917,1452,1755,2012,2247,2342,2651,2966,2995,3371,3431,3553,3569,3574,3910,3936,4053,4071,4088,4170,4254,4314,4478,4678,4938,5168,5744,5922,5999,6000,6310,6430,6645,6659,6733,6926,6938,7048,7056,7223,7296,7323,7358,7375,7691,7705,8111,8544,8562,8728,8837,8925,9145,9267,9321,9639,9645,9666,9690,9734,9829,10023,10048,10114,10243,10351,10509,10673,10725,10801,11077,11278,22943,23012,23081,23118,23129,23143,23215,23231,23328,23568,23576,23635,25852,26052,26575,27332,29798,51601,53918,54566,54733,54801,54843,55008,55222,55601,56937,57161,57509,57580,59277,79029,79772,80312,80758,80830,80833,81688,84620,84830,85480,89795,90853,94240,114907,116843,134429,144699,148979,151613,151987,154075,165215,283208,389336,]   #for testing/running command line



if gene_list:

    gene_list = list(Set(gene_list))

    zscore_percent = 1          
    overlap = 20
    discard_percent = 50
    background_genes=[]
    background_gene_list = []    
    bkgr_check = 0
    bkgr_file = ' '
    unique = 0
    incl_pathways=speed.default_pathways

    #for testing    
##    incl_pathways=['TGFB']
##    unique = 1
##    zscore_percent = 2

    #comment out form gets for testing    
    try:
        zscore_percent = float(form['zscore_percent'].value)
        overlap = float(form['overlap'].value)
        discard_percent = float(form['discard_percent'].value)
        
        if 'unique_check' in form.keys():
            unique=1
        if 'incl_pathways' in form.keys():
            incl_pathways = form.getlist('incl_pathways')
    except:
        print_header()
        print_main()
        print "INVALID Z-SCORE, OVERLAP, DISCARD_PERCENT INPUT, OR NO PATHWAYS SELECTED"
        sys.exit()


        
    if 'background_check' in form.keys() and 'background_file' in form.keys():
        bkgr_check = 1
        conv_gene_list = []
        bkgr_file = form['background_file']
        delim = '\t'

        if bkgr_file.file:
            bkgr_file.file.seek(0)
            for line in bkgr_file.file:
                gene = line.strip().split(delim)[0]
                try:
                    background_genes.append(gene)
                except:
                    pass

            #if the first one is an integer then assume all entrez gene ids
            is_int = 0
            if background_genes:
                try:
                    gene = int(background_genes[0].split(' ')[0])
                    is_int = 1
                except:
                    pass
                    
            if is_int:
                for gene in background_genes:
                    if not gene:
                        continue
                    gene = gene.split(' ')[0]
                    try:
                        background_gene_list.append(int(gene))
                    except:
                        pass
            else:
                for gene in background_genes:
                    if not gene:
                        continue
                    gene = gene.split(' ')[0].split('.')[0]
                    if gene:
                        conv_gene_list.append(gene)

            format_gene_list_bg = ''
            if conv_gene_list:
                if len(conv_gene_list) == 1:
                    conv_gene_list.append('')
                format_gene_list_bg, background_gene_list = speed.convert_gene(conv_gene_list)

                


    if len(gene_list) > 1000:
        gene_list = gene_list[:1000]
    fishers, p_values, fdr, signature_genes_in_list = speed.run_algorithm(gene_list, background_gene_list, zscore_percent, overlap, discard_percent, unique, incl_pathways)
    dload = 0
    if 'dload' in form.keys():
        dload = 1


    print_results(gene_list, fishers, p_values, fdr, signature_genes_in_list, dload, zscore_percent, overlap, discard_percent, unique, bkgr_check, bkgr_file, incl_pathways, format_gene_list)

else:
    print_header()
    print_main()

