#!/usr/bin/python

##This file contains cross validation related code

#Imports
import sqlite3 as sql
import glob
import os, sys
from collections import defaultdict
from sets import Set
from decimal import Decimal
import scipy.stats as stats
from scipy.misc import comb
from scipy import mean
import random
from multiprocessing import Process, Queue, Lock, Pool
import random


#connect to database
con = sql.connect('other scripts/SPEED.db')
cur = con.cursor()


def hypergeom_pmf(m,M,n,N):
    """Returns probability mass function value for hypergeometric distribution with:
    m = # genes of type A in list
    M = # all genes in list
    n = # genes of type A in entire population
    N = # all genes in population

    """

    
    a = Decimal(comb(n, m, exact=1))
    b = Decimal(comb(N-n, M-m, exact=1))
    n = a*b
    d = Decimal(comb(N, M, exact=1))
    
    return float(n/d)

def FDR_per_p_val(fisher_tests):
    """Returns a dictionary with an FDR per possible p-value.
    The input is a dictionary:
    fisher_tests['category']: [m, M, n, N]

    m = # genes of type A in list
    M = # all genes in list
    n = # genes of type A in entire population
    N = # all genes in population
    
    """

    categories = fisher_tests.keys()

    #calculate p-value for each of the categories
    p_value_per_category = {}

    cat_per_p_val = defaultdict(list)
    for cat in categories:
        p_value_per_category[cat] = hypergeom_p_plus(*fisher_tests[cat])        #hypergeometric test per category
        p_value_per_category[cat] =  eval(str(p_value_per_category[cat]))   #there is weird behavior with long floating points 1 = 0.999999999999999999999
        cat_per_p_val[p_value_per_category[cat]].append(cat)


    #assume the p-values are the thresholds (alpha) and calculate the FDR for the p-value
    fdr_per_p_value = {}
    p_values = list(Set(p_value_per_category.values())) #unique set of p-values so we are not iterating through distributions for the same alpha


    for p_val in p_values:
        alpha = p_val   #set alpha cutoff to p_value
        #calculate the expectation of number of false discoveries NFD as the sum of all probabilities that are
        #more extreme than m and less than alpha in all categories
        nfd = 0
        for cat in categories:
            m, M, n, N = fisher_tests[cat]
            prob_test = 0
            for a in range(min(M, n), -1, -1):  #sum all probabilities till alpha is reached
                p = hypergeom_pmf(a,M,n,N)
                if prob_test + p <= alpha:
                    prob_test += p
                else:
                    break
            nfd += prob_test    # add the probability < alpha sum for each category to nfd


        #for all p_values check how many are less than the current p_value/alpha.
        #note there will always be at least 1
        r = 0   #this is the number of positives
        for p_val2 in p_value_per_category.values():    #iterate through all tests
            if p_val2 <= alpha:
                r += 1
            
        fdr = nfd / r
        fdr_per_p_value[alpha] = fdr

    fdr_per_cat = {}
    for p_val in fdr_per_p_value:
        for cat in cat_per_p_val[p_val]:
            fdr_per_cat[cat] = fdr_per_p_value[p_val]
            
    return p_value_per_category, fdr_per_cat

        
def hypergeom_p_plus(m, M, n, N, alpha=1, midP=False):
    """Returns p value for hypergeometric distribution with:
    m = # genes of type A in list
    M = # all genes in list
    n = # genes of type A in entire population
    N = # all genes in population

    """


    p_plus = 0
    start = m
    stop = min(M,n)
    assert start <= stop
    if midP and start + 1 <= stop:
        start += 1

    for a in range(start, stop+1):
##        p_plus += float(stats.hypergeom.pmf(a, N, n, M))
        val = hypergeom_pmf(a,M,n,N)
        p_plus += val
        if p_plus > alpha:
            return None

    if midP:
##        p_plus += 0.5*float(stats.hypergeom.pmf(m, N, n, M))
        p_plus += 0.5*hypergeom_pmf(m,M,n,N)
        if p_plus > alpha:
            return None

    return p_plus

    #process workers per k
def do_work(inQ, outQ, l, full_data, pathways, exps_per_path, paths_per_exp, zscore_percent=5, overlap=50, discard_percent=50, unique=True, zscore_percent_train=5, discard_percent_train=50, fdr_cutoff=0.05):
    while inQ.qsize() > 0:
##        l.acquire()
        training_keys, test_keys = inQ.get(block=False)
        res = the_work(full_data, pathways, exps_per_path, paths_per_exp, training_keys, test_keys, zscore_percent=5, overlap=50, discard_percent=50, unique=True, zscore_percent_train=5, discard_percent_train=50, fdr_cutoff=0.05)
        outQ.put(res)
        print inQ.qsize(), outQ.qsize()
##        l.release()
        print "one_done"
    print "done"


def the_work(full_data, pathways, exps_per_path, paths_per_exp, training_keys, test_keys, zscore_percent=5, overlap=50, discard_percent=50, unique=True, zscore_percent_train=5, discard_percent_train=50, fdr_cutoff=0.05):
    total_background = []
    data = {}
    for key in training_keys:
        total_background += map(lambda x:x[1], full_data[key])
        data[key] = list(Set(map(lambda x:x[1], filter(lambda x: x[0] <= float(zscore_percent)/100, full_data[key]))))
    total_background = len(list(Set(total_background)))
    print total_background
            
    #for each pathway for each experiment keep a list of genes 
    pathway_genes_per_experiment = {}
    gene_counts_per_pathway = {}
    for path in exps_per_path:
        gene_counts_per_pathway[path] = defaultdict(list)
        pathway_genes_per_experiment[path] = {}
        for exp in exps_per_path[path]:
            pathway_genes_per_experiment[path][exp] = []
            if exp not in data:
                continue
            pathway_genes_per_experiment[path][exp] = data[exp]
            for gene in data[exp]:
                gene_counts_per_pathway[path][gene].append(exp)  #will contain redundant entries
            gene_counts_per_pathway[path][gene] = list(Set(gene_counts_per_pathway[path][gene]))

    #take genes per path that are found at least x experiments
    pathway_genes_intersect = {}
    for path in pathway_genes_per_experiment:
        common_num = len(pathway_genes_per_experiment[path]) * float(overlap)/100
        
        pathway_genes_intersect[pathways[path]] = filter(lambda x: len(gene_counts_per_pathway[path][x]) >= common_num, gene_counts_per_pathway[path])
        pathway_genes_intersect[pathways[path]] = list(Set(pathway_genes_intersect[pathways[path]]))    

    #only keep genes that are unique to this pathway
    final_path_genes_intersect = {}
    max_overlap_in_pathways = 1  #unique
    if not unique:
        max_overlap_in_pathways = len(pathway_genes_intersect)  #no change
    
    all_sig_genes = sum(pathway_genes_intersect.values(), [])
    for path in pathway_genes_intersect:
        genes = pathway_genes_intersect[path]
        final_path_genes_intersect[path] = filter(lambda x: all_sig_genes.count(x) <= max_overlap_in_pathways, genes)

    signature_genes_per_pathway = final_path_genes_intersect

    #TEST STAGE        
    #store keys as a tuple of ((exp, replicate), (path_name, path_desc)) and the genes as value
    test_data = {}
    all_test_genes = []
    for key in test_keys:
        test_data[(key, paths_per_exp[key])] = list(Set(map(lambda x:x[1], filter(lambda x: x[0] <= float(zscore_percent_train)/100, full_data[key]))))
        all_test_genes += test_data[(key, paths_per_exp[key])]
    all_test_genes = list(Set(all_test_genes))

    #metrics per path
    TP = defaultdict(int)
    TN = defaultdict(int)
    FP = defaultdict(int)
    FN = defaultdict(int)
    TPR = {}
    FPR = {}


    for exp, true_path in test_data:
##        if str(true_path[0]) not in ['TGFB', 'TNFa']:
##            continue
        genes = test_data[(exp, true_path)]
        random_genes = random.sample(all_test_genes, len(genes))

        p_values = {}

        #collect fisher test information per pathway
        fisher_tests = {}
        signature_genes_in_list = {}
        
        fisher_tests = {}
        p_values = {}
        fdr_per_path = {}
        signature_genes_in_list = {}
            
        for path in signature_genes_per_pathway:
            signature_genes_in_list[path] = Set(genes).intersection(signature_genes_per_pathway[path])
            a = len(signature_genes_in_list[path])  #number of genes IN list AND IN pathway signature set
            b = len(signature_genes_per_pathway[path]) - a  #number of genes IN pathway NOT IN list
            c = len(genes) - a  #number of genes IN list AND NOT IN pathway
            d = total_background - a - b - c    #number of genes NOT IN list AND NOT IN pathway

            m = a
            M = len(genes)
            n = len(signature_genes_per_pathway[path])
            N = total_background              

            fisher_tests[path] = [m, M, n, N]

        if not fisher_tests[true_path][0]:
            print "blah", true_path, fisher_tests[true_path]
            continue
        print true_path, fisher_tests[true_path]

        
        #run multiple hypothesis testing and return two dictionaries
        #p_values contains one-sided p-value from Fisher's Exact Test per pathway
        #fdr_per_path contains the FDR per pathway
        p_values, fdr_per_path = FDR_per_p_val(fisher_tests)
        print path, fisher_tests[path]
        print


    #rank based:
        top_rank_path = defaultdict(list)
        for path in fdr_per_path:
            top_rank_path[fdr_per_path[path]].append(path)
        keys = top_rank_path.keys()
        keys.sort()
        top_fdr = keys[0]
        top_paths = top_rank_path[top_fdr]
        

        #if top path has an FDR > 5%
        #FN for true path += 1
        #TN for all other paths += 1
        if top_fdr > fdr_cutoff:
            FN[true_path] += 1
            for path in fdr_per_path:
                if path != true_path:
                    TN[path] += 1                
            continue

        #if the top ranking path is the true path:
        #TP for top path += 1
        #TN for all other paths += 1
        #else
        #FP for top path += 1
        #FN for true path += 1
        if true_path in top_paths:
            TP[true_path] +=1
            for path in fdr_per_path:
                if path not in top_paths:
                    TN[path] += 1  
        else:
            for top_path in top_paths:                
                FP[top_path] += 1
            FN[true_path] +=1


        #FDR based: # assumes no crosstalk       

        #if true_path has an FDR < 5%
        #TP[true_path] += 1
        #else:
        #FN[true_path] += 1
        #for other paths
        #if FDR < 5%
        #FP[path] += 1
        #else:
        #TN[path] += 1
##        for path in fdr_per_path:
##            if path == true_path:
##                if fdr_per_path[path] <= fdr_cutoff:
##                    TP[path] += 1
##                else:
##                    FN[path] += 1
##            else:
##                if fdr_per_path[path] <= fdr_cutoff:
##                    FP[path] += 1
##                else:
##                    TN[path] += 1

##    for exp, true_path in test_data:
##        if not (TP[true_path] + FN[true_path]):
##            TPR[true_path] = 0
##            continue
##        print true_path, TP[true_path], FN[true_path]
##        print true_path, TN[true_path], FP[true_path]
##        TPR[true_path] = float(TP[true_path]) / (TP[true_path] + FN[true_path])
##        if not (TN[true_path] + FP[true_path]):
##            FPR[true_path] = 0
##            continue
##        FPR[true_path] = float(FP[true_path]) / (TN[true_path] + FP[true_path])

    TP = sum(TP.values())
    FP = sum(FP.values())
    TN = sum(TN.values())
    FN = sum(FN.values())
        
    TPR = float(TP) / (TP+FN)
    FPR = float(FP) / (FP+TN)
    return TPR, FPR        
   

if __name__ == "__main__":    
    k = int(sys.argv[1])
    zscore_percent = 5
    overlap=50
    discard_percent=50
    if len(sys.argv) >= 5:
        discard_percent = float(sys.argv[4])    
    if len(sys.argv) >= 4:
        overlap = float(sys.argv[3])
    elif len(sys.argv) >= 3:
        zscore_percent = float(sys.argv[2])


    pathways = {}
    q = cur.execute('SELECT * FROM Pathways').fetchall()
    for pid, name in q:
        pathways[pid] = (pid, name)

    
    #keep list of genes in dictionary
    db_genes = dict(cur.execute('SELECT * FROM Genes'))

    #for each pathway store experiment
    exps_per_path = defaultdict(list)
    paths_per_exp = {}
    for e_id, p_id in cur.execute('SELECT e_id, p_id FROM Experiments'):
        exps_per_path[p_id].append(e_id)
        paths_per_exp[e_id] = pathways[p_id]


    
    full_data = defaultdict(list)

    #collect all genes per experiment with a abs(z-score) greater than threshold
    for e_id, g_id, abs_zvalue_percent in cur.execute('SELECT e_id, g_id, abs_zvalue_percent FROM Data WHERE expression_percent <= ?', (float(discard_percent)/100,)):
        if e_id not in paths_per_exp:
            continue
        full_data[e_id].append((abs_zvalue_percent, g_id))


    #sort by zvalue
    for e_id in full_data:
        full_data[e_id].sort()


    #generate k random data sets
    data_keys = full_data.keys()
    #shuffle randomly
    random.shuffle(data_keys)
    #split into k sets
    step = int(round(float(len(data_keys))/k))
    data_sets = []
    cumsum = 0
    for i in range(k-1):
        data_sets.append(data_keys[cumsum:cumsum+step])
        cumsum += step
    data_sets.append(data_keys[cumsum:])

        

    #metrics
    TPRs = defaultdict(list)
    FPRs = defaultdict(list)

    #start k processes
    q1 = Queue()
    q2 = Queue()
    lock = 1

    #split into training and test
    for i in range(k):
        #TRAINING STAGE
        #training set
        training_keys = sum([data_sets[j] for j in range(len(data_sets)) if j != i], [])
        test_keys = data_sets[i]
        q1.put((training_keys, test_keys))

    
    processes = [Process(target=do_work, args=(q1,q2,lock, full_data, pathways, exps_per_path, paths_per_exp, zscore_percent, overlap, discard_percent)) for i in range(k)]
    print processes
    for p in processes:
        print "s"
        p.start()
    for p in processes:
        p.join()

    results = []
    TPRs = []
    FPRs = []
    while q2.qsize() > 0:
        TPR, FPR = q2.get()
        
        TPRs.append(TPR)
        FPRs.append(FPR)

    #average TPR, FPR
    avg_TPR = mean(TPRs)
    avg_FPR = mean(FPRs)
    print avg_TPR, avg_FPR
##    
##    for path in TPRs:
##        avg_TPR[path] = mean(TPRs[path])
##        avg_FPR[path] = mean(FPRs[path])
##        print path, avg_TPR[path], avg_FPR[path]





    
    
    






    
