#!/usr/bin/python

##This file contains cgi code

#Imports
import cgi
import speed_functions as speed
from sets import Set
import urllib

def print_header():
    """This function prints the main HTML content in the page

    """
    
    print "Content-Type: text/html\n\n"

    print """
<HEAD>
    <LINK href="/SPEED_style.css" rel="stylesheet" type="text/css">
    <LINK href="/favicon.ico" rel="shortcut icon">

    <script type="text/javascript">
    function send_email(form)
    {
        var bodytxt = form.pathway.value;
        if (form.pathway.value=='other')
        {
        bodytxt = form.new_pathway.value;
        }

        var email = "speed@sys-bio.net"; 
        var subject = "SPEED Suggest Pathway";
        var info = form.information.value;
        bodytxt = "Pathway:\\n" + bodytxt + "\\n\\n" + "Information:\\n" + info;
        bodytxt = escape(bodytxt)

        var mailto_link = 'mailto:'+email+'?subject='+subject+'&body='+bodytxt;
        win = window.open(mailto_link,'emailWindow'); 
        if (win && win.open &&!win.closed)
        {
            win.close();
        }

    }

    function drop_down_select(form)
    {
        if (form.pathway.value=='other')
        {
            form.new_pathway.disabled=false;
        }
        else
        {
            form.new_pathway.disabled=true;
        }        
    }    
    </script>
    
</HEAD>
      
<title>SPEED: Suggest Pathways</title>
<body>
<h1 align=center>SPEED: (S)ignaling (P)athway (E)nrichment using (E)xperimental (D)atasets</h1>
</body>

<h2>
<table border=1 align="center" style="font-size:22px;" cellspacing=8>
<tr>
<td style="background-color:#eeeeff;"><a href="/" style="color:#333333;" >&nbsp;Home&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/cgi-bin/run_speed.py" style="color:#333333;" >&nbsp;Run SPEED&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/cgi-bin/download_annotations.py" style="color:#333333;" >&nbsp;Download Annotations&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/cgi-bin/database_statistics.py" style="color:#333333;" >&nbsp;Database Statistics&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/cgi-bin/suggest.py" style="color:#333333;" >&nbsp;Suggest Pathways&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/help.html" style="color:#333333;" >&nbsp;Help&nbsp;</a></td>
</tr>
</table>
</h2>


"""



def print_main(pathway='', information=''):
    """This function prints the main HTML content in the page

    """

    all_pathways = speed.pathways.keys()
    all_pathways.sort()
    
    if not pathway:
        pathway = all_pathways[0]

    print """    

<p>
Please use this form to suggest a new data set or pathway. The form will use your default email client.
</p>
<hr>

<FORM METHOD="post">
<p align="center">
"""
    print "<table border=0>"

    print '<tr><td><b>Select Pathway:</b><br>or suggest new pathway in text box</td>'
    print '<td><select name="pathway" onchange="drop_down_select(this.form)">'
    for path in all_pathways:
        if path == pathway:
            print '<option value="%s" selected>%s</option>' % (path, path)
        else:
            print '<option value="%s" selected>%s</option>' % (path, path)
    print '<option value="other">other, please specify:</option>'
    print '</select>'
    print '<input type="text" name="new_pathway" disabled="disabled"></td></tr>'

    print '<tr><td><b>Enter information</b><br>as GEO accessions (GSE, GSM, or GDS), hyperlinks,<br>or publications to suggest inclusion of data</td>'
    print '<td><TEXTAREA NAME="information" COLS="37" ROWS="10">%s</TEXTAREA></td></tr>' % information
    print '</table><br>'

    print """
    <input onClick="send_email(this.form)" type="button" value="Submit">
   
</p>
</FORM>
<hr>
<br>
""" 

    
print_header()
print_main()
