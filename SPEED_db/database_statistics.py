#!/usr/bin/python

##This file contains cgi code

#Imports
import cgi
import re
import speed_functions as speed
from collections import defaultdict
from sets import Set

def print_header():
    """This function prints the main HTML content in the page

    """
    
    print "Content-Type: text/html\n\n"

    print """
<HEAD>
    <LINK href="/SPEED_style.css" rel="stylesheet" type="text/css">
    <LINK href="/favicon.ico" rel="shortcut icon"> 
</HEAD>
      
<title>SPEED: Database Statistics</title>
<body>
<h1 align=center>SPEED: (S)ignaling (P)athway (E)nrichment using (E)xperimental (D)atasets</h1>
</body>

<h2>
<table border=1 align="center" style="font-size:22px;" cellspacing=8>
<tr>
<td style="background-color:#eeeeff;"><a href="/" style="color:#333333;" >&nbsp;Home&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/cgi-bin/run_speed.py" style="color:#333333;" >&nbsp;Run SPEED&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/cgi-bin/download_annotations.py" style="color:#333333;" >&nbsp;Download Annotations&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/cgi-bin/database_statistics.py" style="color:#333333;" >&nbsp;Database Statistics&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/cgi-bin/suggest.py" style="color:#333333;" >&nbsp;Suggest Pathways&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/help.html" style="color:#333333;" >&nbsp;Help&nbsp;</a></td>
</tr>
</table>
</h2>


"""

def print_tail():
    """This function prints the tail of the web page

    """

    print """
    <a href="http://www.sys-bio.net/">Group of Systems Biology of Molecular Network</a>. 
    Contact: <a href="nils.bluethgen@charite.de">Nils Bl&#252;thgen</a>.
    <br>
    Laboratory of Molecular Tumor Pathology (Charite) and the Institute of Theoretical Biology (HU Berlin).
    <br>

"""    


def print_main():
    """This function prints the main HTML content in the page

    """
    

    print """    

<p>
The SPEED database stores z-score normalized microarray data from perturbation experiments for specific signaling pathways.
All microarray data is downloaded from the Gene Expression Omnibus (<a href="http://www.ncbi.nlm.nih.gov/geo/" target="_blank">GEO</a>) at NCBI.
The GEO series for the perturbation experiments are noted per pathway.
</p>

<hr>
"""

def print_results(pathway=None):

    cur = speed.cur
	
    print "<p><table width=85% border=1 align=center>"
    
    #number of genes    
    num_genes = cur.execute('SELECT COUNT(*) FROM Genes').fetchall()[0]
    print "<tr><td><b>Number of Genes: </b></td><td>%d</td></tr>" % num_genes
    
    #number of data points   
    num_data = cur.execute('SELECT COUNT(*) FROM Data').fetchall()[0]
    print "<tr><td><b>Number of Measurements: </b></td><td>%d</td></tr>" % num_data
    
    #for each pathway store experiment/replicates
    exps_per_paths = defaultdict(list)
    for e_id, p_id, controls_GSM, stimuli_GSM, cell_line, modification, time, comment in cur.execute('SELECT * FROM Experiments'):
        exps_per_paths[p_id].append((e_id, controls_GSM, stimuli_GSM, cell_line, modification, time, comment))


    
    #number of experiments per pathway
    for pid in exps_per_paths:
        print "<tr><td><b><a href='/cgi-bin/database_statistics.py?pathway=%s'>%s</a> Experiments: </b></td><td>%d<br>" % (pid, pid, len(exps_per_paths[pid]))
        print ', '.join(list(Set(["<a href='http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=%s'  target='_blank'>%s</a>" % (re.match('(GSE\d+).*', vals[0]).groups()[0], re.match('(GSE\d+).*', vals[0]).groups()[0]) for vals in exps_per_paths[pid]])))
        print "</td></tr>"

    print "</table></p><br>"

    if pathway:
        print "<hr><br>"
        print "<h3 style='padding-right:35px; padding-left:35px; padding-top:10px; padding-bottom:10px;'>%s Data Details</h3>" % pathway
        details = exps_per_paths[pathway]
        details.sort()
        print "<p>"
        for e_id, controls_GSM, stimuli_GSM, cell_line, modification, time, comment in details:
            print "<b>%s</b><br>" % e_id
            print "<table border=0>"
            print "<tr><td>Cell Line:</td><td>%s</td></tr>" % cell_line
            print "<tr><td>Modification:</td><td>%s</td></tr>" % modification
            print "<tr><td>Time:</td><td>%s</td></tr>" % time
            print "<tr><td>Control Samples:</td><td>"
            print ', '.join(["<a href='http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=%s'  target='_blank'>%s</a>" % (gsm, gsm) for gsm in controls_GSM.split(',')])
            print "</td></tr>"
            print "<tr><td>Experimental Samples:</td><td>"
            print ', '.join(["<a href='http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=%s'  target='_blank'>%s</a>" % (gsm, gsm) for gsm in stimuli_GSM.split(',')])
            print "</td></tr>"
            print "</table><br>"
            
        print "</p>"        


    
print_header()
print_main()

form = cgi.FieldStorage()
if form:
    try:
        print_results(pathway=form['pathway'].value)
    except:
        print "INVALID INPUT"

else:
    print_results(pathway=None)
