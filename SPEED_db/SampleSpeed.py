#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 11:12:21 2019

@author: tony
"""

import speed_functions as speed
# inputs gene_list and background_genes are lists of integers in Entrez Gene ID Format
# if background_genes is an empty list, all genes in the SPEED database are considered
gene_list = [102,240,393,677,1263,2323,2771,2956,3066,3303]
background_genes = []

# parameters to extract signature genes controlling differential expression, overlap across experiments, and minimum expression level
zscore_percent = 1.0
overlap = 20.0
discard_percent = 50.0

# 1 if unique constraint, 0 otherwise
unique = 0

# speed.default_pathways lists the 4 default pathways, or specify pathways to restrict search 
incl_pathways = ['TGFB', 'TLR', 'MAPK_PI3K', 'JAK-STAT']

# run SPEED algorithm
fishers, p_values, fdr, signature_genes_in_list = speed.run_algorithm(gene_list, background_genes, zscore_percent, overlap, discard_percent, unique, incl_pathways)

# code returns dictionaries with pathways as keys and (1) fishers tests, (2) p-values, (3) fdr, and (4) signature genes in input list as values.

print(fishers)