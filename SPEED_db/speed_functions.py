#!/usr/bin/python

##This file contains annotation enrichment calculation code for the SPEED project

#Imports
import sqlite3 as sql
import glob
import os, sys
from collections import defaultdict
#from sets import Set
from decimal import Decimal
import scipy.stats as stats
from scipy.misc import comb
import six; from six.moves import cPickle as pickle
import  marshal

#connect to database
con = sql.connect('other scripts/SPEED.db')
cur = con.cursor()

default_pathways = ['JAK-STAT', 'TGFB', 'MAPK_PI3K', 'TLR']

#get all pathway names
pathways = {}
q = cur.execute('SELECT * FROM Pathways').fetchall()
for pid, name in q:
    pathways[pid] = (pid, name)




def hypergeom_pmf(m,M,n,N):
    """Returns probability mass function value for hypergeometric distribution with:
    m = # genes of type A in list
    M = # all genes in list
    n = # genes of type A in entire population
    N = # all genes in population

    """

    return float(Decimal(comb(n, m, exact=1) * comb(N-n, M-m, exact=1)) / Decimal(comb(N, M, exact=1)))

def FDR_per_p_val(fisher_tests):
    """Returns a dictionary with an FDR per possible p-value.
    The input is a dictionary:
    fisher_tests['category']: [m, M, n, N]

    m = # genes of type A in list
    M = # all genes in list
    n = # genes of type A in entire population
    N = # all genes in population
    
    """

    categories = fisher_tests.keys()

    #calculate p-value for each of the categories
    p_value_per_category = {}

    cat_per_p_val = defaultdict(list)
    for cat in categories:
        p_value_per_category[cat] = hypergeom_p_plus(*fisher_tests[cat])        #hypergeometric test per category
##        p_value_per_category[cat] =  eval(str(p_value_per_category[cat]))   #there is weird behavior with long floating points 1 = 0.999999999999999999999
        cat_per_p_val[p_value_per_category[cat]].append(cat)


    #assume the p-values are the thresholds (alpha) and calculate the FDR for the p-value
    fdr_per_p_value = {}
    p_values = list(set(p_value_per_category.values())) #unique set of p-values so we are not iterating through distributions for the same alpha


    for p_val in p_values:
        alpha = p_val   #set alpha cutoff to p_value
        #calculate the expectation of number of false discoveries NFD as the sum of all probabilities that are
        #more extreme than m and less than alpha in all categories
        nfd = 0
        for cat in categories:
            m, M, n, N = fisher_tests[cat]
            prob_test = 0
            for a in range(min(M, n), -1, -1):  #sum all probabilities till alpha is reached
                p = hypergeom_pmf(a,M,n,N)
                if prob_test + p <= alpha:
                    prob_test += p
                else:
                    break
            nfd += prob_test    # add the probability < alpha sum for each category to nfd

        #for all p_values check how many are less than the current p_value/alpha.
        #note there will always be at least 1
        r = 0   #this is the number of positives
        for p_val2 in p_value_per_category.values():    #iterate through all tests
            if p_val2 <= alpha:
                r += 1
        
        fdr = nfd / r
        fdr = min(fdr, 1.0)
        fdr_per_p_value[alpha] = fdr

    fdr_per_cat = {}
    for p_val in fdr_per_p_value:
        for cat in cat_per_p_val[p_val]:
            fdr_per_cat[cat] = fdr_per_p_value[p_val]

    
    return p_value_per_category, fdr_per_cat

        
def hypergeom_p_plus(m, M, n, N, alpha=1, midP=False):
    """Returns p value for hypergeometric distribution with:
    m = # genes of type A in list
    M = # all genes in list
    n = # genes of type A in entire population
    N = # all genes in population

    """


    p_plus = 0
    start = m
    stop = min(M,n)
    assert start <= stop
    if midP and start + 1 <= stop:
        start += 1

##    for a in range(start, stop+1):
    for a in range(stop, start-1, -1):
##        p_plus += float(stats.hypergeom.pmf(a, N, n, M))
        val = hypergeom_pmf(a,M,n,N)
        p_plus += val
        if p_plus > alpha:
            return 1.0

    if midP:
##        p_plus += 0.5*float(stats.hypergeom.pmf(m, N, n, M))
        p_plus += 0.5*hypergeom_pmf(m,M,n,N)
        if p_plus > alpha:
            return 1.0

    return p_plus

def signature_genes(zscore_percent=5, overlap=50, discard_percent=50, unique=True, incl_pathways=default_pathways):
    """This function searches database and returns:
    dictionary of signature genes per pathway, and
    a dictionary with all genes and their names in background

    Ex:
    signature_genes_per_pathway, db_genes = signature_genes()
    signature_genes_per_pathway[path] = [gene1, gene2, ...]

    """    


    if not incl_pathways:
        incl_pathways = pathways.keys()

    #keep list of genes in dictionary
    db_genes = dict(cur.execute('SELECT * FROM Genes'))

    #for each pathway store experiment
    exps_per_paths = defaultdict(list)
    incl_exps = []
    for e_id, p_id in cur.execute('SELECT e_id, p_id FROM Experiments'):
        if p_id in incl_pathways:
            exps_per_paths[p_id].append(e_id)
            incl_exps.append(e_id)

    data = defaultdict(list)
    
    #try doing this all in memory. If it not efficient, then we can run separate queries per pathway (and per experiment) - also not efficient    
    for e_id, g_id in cur.execute('SELECT e_id, g_id FROM Data \
    WHERE abs_zvalue_percent <= ? AND expression_percent <= ?', (str(float(zscore_percent)/100), str(float(discard_percent)/100))):
        data[e_id].append(g_id)

    for e_id in list(data.keys()):
        if e_id not in incl_exps:
            del data[e_id]


    #for each pathway for each experiment keep a list of genes with a abs(z-score) greater than threshold
    pathway_genes_per_experiment = {}
    gene_counts_per_pathway = {}
    for path in incl_pathways:
        gene_counts_per_pathway[path] = defaultdict(list)
        pathway_genes_per_experiment[path] = {}
        for exp in exps_per_paths[path]:
            pathway_genes_per_experiment[path][exp] = []
            for gene in data[exp]:
                pathway_genes_per_experiment[path][exp].append(gene)
                gene_counts_per_pathway[path][gene].append(exp)  #will contain redundant entries
            pathway_genes_per_experiment[path][exp] = list(set(pathway_genes_per_experiment[path][exp]))

##    print pathway_genes_per_experiment
    
    #take genes per path that are found at least in half of the experiments
    pathway_genes_intersect = {}
    for path in pathway_genes_per_experiment:
##        common_num = len(pathway_genes_per_experiment[path])                #all
##        common_num = len(pathway_genes_per_experiment[path]) / 2            #half
##        common_num = len(pathway_genes_per_experiment[path]) -1            #all -1
        common_num = len(pathway_genes_per_experiment[path]) * float(overlap)/100
        
        
        pathway_genes_intersect[pathways[path]] = []
        for gene in gene_counts_per_pathway[path]:
            if len(set(gene_counts_per_pathway[path][gene])) >= common_num:
                pathway_genes_intersect[pathways[path]].append(gene)

        pathway_genes_intersect[pathways[path]] = list(set(pathway_genes_intersect[pathways[path]]))
    
    #only keep genes that are unique to this pathway
    final_path_genes_intersect = {}
    max_overlap_in_pathways = 1  #unique
    if not unique:
        max_overlap_in_pathways = len(pathway_genes_intersect)  #no change
    
    all_sig_genes = sum(pathway_genes_intersect.values(), [])
    for path in pathway_genes_intersect:
        genes = pathway_genes_intersect[path]
        final_path_genes_intersect[path] = [gene for gene in genes if all_sig_genes.count(gene) <= max_overlap_in_pathways]

    return final_path_genes_intersect, db_genes

def run_algorithm(genes, background_genes=[], zscore_percent=5, overlap=50, discard_percent=50, unique=True, incl_pathways=default_pathways):
    """This function takes a list of genes and returns (p_values, fdr) as a tuple of two dictionaries

    m = # genes of type A in list
    M = # all genes in list
    n = # genes of type A in entire population
    N = # all genes in population
    
    """

    #get signature genes
    signature_genes_per_pathway, db_genes = signature_genes(zscore_percent, overlap, discard_percent, unique, incl_pathways)
    
    p_values = {}

    #collect fisher test information per pathway
    fisher_tests = {}
    signature_genes_in_list = {}
    if not background_genes:
        background_genes = db_genes.keys()

    total_background = len(set(db_genes.keys()).intersection(background_genes))
    
    fisher_tests = {}
    p_values = {}
    fdr_per_path = {}
    signature_genes_in_list = {}
    if total_background <= 1:
        return fisher_tests, p_values, fdr_per_path, signature_genes_in_list
        
    for path in signature_genes_per_pathway:
        #only consider signature genes in background
        signature_genes_per_pathway[path] = set(signature_genes_per_pathway[path]).intersection(background_genes)
        
        signature_genes_in_list[path] = set(genes).intersection(signature_genes_per_pathway[path])

        m = len(signature_genes_in_list[path])  #number of genes IN list AND IN pathway signature set
        M = len(set(genes).intersection(background_genes))      #number of genes IN list (AND IN background)
        n = len(signature_genes_per_pathway[path])    #number of genes IN pathway signature set (AND IN background)
        N = total_background

        fisher_tests[path] = [m, M, n, N]

    #run multiple hypothesis testing and return two dictionaries
    #p_values contains one-sided p-value from Fisher's Exact Test per pathway
    #fdr_per_path contains the FDR per pathway
    p_values, fdr_per_path = FDR_per_p_val(fisher_tests)

    for path in signature_genes_in_list:
        signature_genes_in_list[path] = [(gene, db_genes[gene]) for gene in signature_genes_in_list[path]]
    
    return fisher_tests, p_values, fdr_per_path, signature_genes_in_list
    

def convert_gene(conv_gene_list):
    cur.execute('SELECT g_id, UPPER(conversion) FROM ConvertGenes WHERE UPPER(conversion) IN %s' % str(tuple([r.upper() for r in conv_gene_list])))
    format_gene_list = ''
    gene_list = []
    for row in cur.fetchall():
        gene_list.append(row[0])
        format_gene_list += '%d (%s)\n' % (row[0], row[1])
    
    return format_gene_list, gene_list    

if __name__ == "__main__":
    pass
    
