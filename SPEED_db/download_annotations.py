#!/usr/bin/python

##This file contains cgi code

#Imports
import cgi
import speed_functions as speed
from sets import Set

def print_header():
    """This function prints the main HTML content in the page

    """
    
    print "Content-Type: text/html\n\n"

    print """
<HEAD>
    <LINK href="/SPEED_style.css" rel="stylesheet" type="text/css">
    <LINK href="/favicon.ico" rel="shortcut icon"> 
</HEAD>
      
<title>SPEED: Download Annotations</title>
<body>
<h1 align=center>SPEED: (S)ignaling (P)athway (E)nrichment using (E)xperimental (D)atasets</h1>
</body>

<h2>
<table border=1 align="center" style="font-size:22px;" cellspacing=8>
<tr>
<td style="background-color:#eeeeff;"><a href="/" style="color:#333333;" >&nbsp;Home&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/cgi-bin/run_speed.py" style="color:#333333;" >&nbsp;Run SPEED&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/cgi-bin/download_annotations.py" style="color:#333333;" >&nbsp;Download Annotations&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/cgi-bin/database_statistics.py" style="color:#333333;" >&nbsp;Database Statistics&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/cgi-bin/suggest.py" style="color:#333333;" >&nbsp;Suggest Pathways&nbsp;</a></td>
<td style="background-color:#eeeeff;"><a href="/help.html" style="color:#333333;" >&nbsp;Help&nbsp;</a></td>
</tr>
</table>
</h2>


"""



def print_main():
    """This function prints the main HTML content in the page

    """
    

    print """    

<p>
All signature genes may be downloaded using this form.
</p>
<hr>

<FORM METHOD="POST" ACTION="/cgi-bin/download_annotations.py">
<p align="center">
"""
    print "<table border=0>"
    incl_pathways = speed.default_pathways
    all_pathways = incl_pathways + list(Set(speed.pathways.keys()).difference(incl_pathways))
    print '<tr><td>Pathways to include:</td><td><ul class="checklist">'
    for path in all_pathways:
        path_text = ''
        if path in incl_pathways:
            path_text = 'checked'
        print '<li><input type="checkbox" name="incl_pathways" value="%s" %s>%s</li>' % (path, path_text, path)

    print '</ul></td></tr>'

    print """    
<tr>
<td>Max. absolute z-score percentile:</td>
<td><input type="text" name="zscore_percent" value=1> &#37;</td>
</tr>
<tr>
<td>Min. percent overlap across experiments:</td>
<td><input type="text" name="overlap" value=20> &#37;</td>
</tr>
<tr>
<td>Max. expression level percentile:</td>
<td><input type="text" name="discard_percent" value=50> &#37;</td>
</tr>
<tr>
<td>Signature genes must be unique:</td>
<td><input type="checkbox" name="unique_check" value=1></td>
</tr>
</table>
<br>

<INPUT TYPE="submit" name="run">
</p>
</FORM>
<hr>
<p>
<a href="/SPEED_db.zip">Click here to download entire SPEED SQLite database and python scripts.</a>
<br>
<br>
<a href="/SPEED_tables.zip">Click here to download SPEED database tables in tab delimited text.</a>
<br>


""" 

def print_results(zscore_percent, overlap, discard_percent, unique, incl_pathways):
    
    print "Content-Type: text/plain\n"
    print "#Pathway\tGene_ID\tGene_Name"

    import speed_functions as speed

    annotations, db_genes = speed.signature_genes(zscore_percent, overlap, discard_percent, unique, incl_pathways)
    for path in annotations:
        for gene in annotations[path]:
            print "%s\t%d\t%s" % (path[0], gene, db_genes[gene])           


##import sys
##print_results(1.0, 20.0, 50)
##sys.exit()

form = cgi.FieldStorage()
if form:
    try:
        unique = 0
        if 'unique_check' in form.keys():
            unique = 1
        incl_pathways = speed.default_pathways
        if 'incl_pathways' in form.keys():
            incl_pathways = form.getlist('incl_pathways')
        print_results(float(form['zscore_percent'].value), float(form['overlap'].value), float(form['discard_percent'].value), unique, incl_pathways)
    except:
        print_header()
        print_main()
        print "INVALID INPUT"

else:
    print_header()
    print_main()
